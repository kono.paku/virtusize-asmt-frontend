// Load from source code
import User from '../types/User';
import ActionType from '../types/ActionType';

type PanelAction = {
  type: ActionType,
  payload: User
}

export default PanelAction;