export type StrengthIndicatorProps = {
  score: number;
}

export default StrengthIndicatorProps;