type User = {
  emailAddress: string;
  firstName: string;
  lastName: string;
  password: string;
}

export default User;