// Load from source code
import User from './User';

type DescriptionProps = {
  userProfile: User;
  onEdit: () => void;
}

export default DescriptionProps;