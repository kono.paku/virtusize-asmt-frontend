type ButtonProps = {
  type?: 'button' | 'submit' | 'reset';
  onClick?: () => void;
  marginTop: string;
  children: string;
}

export default ButtonProps;