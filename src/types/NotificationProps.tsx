type NotificationProps = {
  errorMessage: string;
}

export default NotificationProps;