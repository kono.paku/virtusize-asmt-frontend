// Load from source code
import User from './User';

type PanelState = {
  user: User;
  status: string;
}

export default PanelState;