// Load from source code
import User from './User';

type InputFormProps = {
  userProfile: User;
  onSubmit: (user: User) => void;
}

export default InputFormProps;