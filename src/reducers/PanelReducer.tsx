// Load from packages
import React from 'react';

// Load from source code
import ActionType from '../types/ActionType';
import PanelState from '../types/PanelState';
import PanelAction from '../actions/PanelAction';

  /**
    * Get a Gavatar URL from email address
    * 
    * @param {PanelState} currentPanel PanelState before updated
    * @param {PanelAction} panelAction PanelAction to be handled
    * @return {PanelState} PanelState after updated
    */
const PanelReducer: React.Reducer<PanelState, PanelAction> = (currentPanel: PanelState, panelAction: PanelAction) => {
  switch (panelAction.type) {
    case ActionType.RETRIEVING:
      return {
        ...currentPanel,
        user: {
          ...panelAction.payload
        },
        status: 'LOADING',
      };

    case ActionType.RETRIEVED:
      return {
        ...currentPanel,
        user: {
          ...panelAction.payload
        },
        status: 'NOT_EDITABLE',
      };
    case ActionType.MODIFYING:
      return {
        ...currentPanel,
        status: 'EDITABLE'
      };
    case ActionType.SUBMITTING:
      return {
        ...currentPanel,
        status: 'LOADING',
      };
    case ActionType.SUBMITTED:
      return {
        user: {
          ...panelAction.payload
        },
        status: 'NOT_EDITABLE',
      };
    case ActionType.FAILED:
      return {
        ...currentPanel,
        status: 'RETRY',
      };  
    default:
      throw new Error('Should not get there!');
  }
}; 

export default PanelReducer;