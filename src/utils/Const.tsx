/** Class representing Const **/
class Const {
  public static readonly GRAVATARS_ENDPOINT = 'https://s.gravatar.com/avatar/';
  public static readonly FIREBASE_ENDPOINT = 'https://virtualsize-asmt-frontend-default-rtdb.firebaseio.com/userInfo/-LiqmAeJZmNpx1dt72u0.json';
  public static readonly PASSWORD_MIN_LENGTH = 4;
  public static readonly NAME_MIN_LENGTH = 3;
  public static readonly NAME_MAX_LENGTH = 10;
  public static readonly STRENGTH_CRITERIA = 3;
  public static readonly SHORT_MSG = 'Too Short!';
  public static readonly LONG_MSG = 'Too Long!';
  public static readonly REQUIRED_MSG = 'Please input this field.';
  public static readonly INVALID_EMAIL_MSG = 'Please input a valid email';
  public static readonly HTTP_GET_ERROR = 'HTTP_GET_ERROR';
  public static readonly HTTP_PUT_ERROR = 'HTTP_PUT_ERROR';
}

export default Const;