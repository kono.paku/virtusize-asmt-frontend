// Load from packages
import md5 from 'md5';

// Load from source code
import Const from './Const'

/** Class representing ImageUtil **/
class ImageUtil {
  /**
    * Get a Gavatar URL from email address
    * 
    * @param {string} emailAddress Email address to be converted to md5 hashed
    * @return {string} Gravatars URL
    */
  public static getGravatarsUrl(emailAddress: string): string {

    return Const.GRAVATARS_ENDPOINT + md5(emailAddress)

  }

}

export default ImageUtil;