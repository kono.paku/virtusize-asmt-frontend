import React from 'react';
import './App.scss';
import Panel from './components//Panel';

const App = () => {
  return ( 
    <div className="app">
      <Panel/>
    </div>
)};

export default App;
