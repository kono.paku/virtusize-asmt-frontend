// Load from source code
import Const from '../utils/Const';
import { StrengthIndicatorProps } from '../types/StrengthIndicatorProps';
import './StrenghIndicator.scss';

/**
 * Functional component to show an indicator text for password strength
 * 
 * @param {StrengthIndicatorProps} props - property values from the parents
 */
const StrenghIndicator = (props: StrengthIndicatorProps) => {
  let strenghIndicator;
  if(props.score >= Const.STRENGTH_CRITERIA) {
    strenghIndicator = <div className='indicator--strong'>Strong</div>;
  } else {
    strenghIndicator = <div className="indicator--weak">Weak</div>;
  }

  return (
    <>
      {strenghIndicator}
    </>
  );
};

export default StrenghIndicator;
