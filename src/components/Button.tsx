// Load from source code
import ButtonProps from '../types/ButtonProps';
import './Button.scss';

const Button = (props: ButtonProps) => {

  return (
    <button
      type={props.type}
      onClick={props.onClick}
      className="button"
      style={{marginTop: `${props.marginTop}`}}>
        {props.children}
    </button>
  );
};

export default Button;
