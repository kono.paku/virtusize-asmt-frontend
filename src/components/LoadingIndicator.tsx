// Load from source code
import './LoadingIndicator.scss';

/**
 * Functional component to show a loading indicator on http request
 */
const LoadingIndicator = () => {
  return (
    <div className="loadingindicator">
      <div />
      <div />
      <div />
      <div />
    </div>
  );
};

export default LoadingIndicator;
