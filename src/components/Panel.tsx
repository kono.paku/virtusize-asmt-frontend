// Load from packages
import React, { useEffect, useCallback, useReducer, useMemo, useState } from 'react';
import axios from 'axios';

// Load from source code
import PanelState from '../types/PanelState';
import PanelAction from '../actions/PanelAction';
import ActionType from '../types/ActionType';
import PanelReducer from '../reducers/PanelReducer';
import User from '../types/User';
import InputForm from './InputForm';
import Description from './Description';
import ProfileImage from './ProfileImage';
import LoadingIndicator from './LoadingIndicator';
import Button from './Button';
import Notification from './Notification';
import Const from '../utils/Const';
import './Panel.scss';

const initialPanelState: PanelState = {
  user: {
    emailAddress: '',
    firstName: '',
    lastName: '',
    password: ''  
  },
  status: ''
}

/**
 * Functional component to show a Panel
 */
const Panel = () => {
  let panelTopContent;
  let panelBottomContent;
  const [panelState, dispatchPanelState] = useReducer<React.Reducer<PanelState, PanelAction>>(PanelReducer, initialPanelState);
  const [errorState, setErrorState] = useState({status: '', message: ''});

  /**
   * Function calling HTTP GET request to retreive user information
   */
  const retriveUserInfo = useCallback(
    () => {
      axios({
        method: 'GET',
        url: `${Const.FIREBASE_ENDPOINT}`
      }).then((response) => {
        dispatchPanelState({
          type: ActionType.RETRIEVED,
          payload: response.data
        }
      )}).catch((error) => {
        dispatchPanelState({
          type: ActionType.FAILED,
          payload: panelState.user
        });

        setErrorState({
          status: Const.HTTP_GET_ERROR,
          message: error.message
        });
      });  
    }
  , []);

  /**
   * Function calling HTTP PUT request to update user information
   * 
   * @param {object: User} userProfile - User information to update
   */
  const updateUserInfo = useCallback(
    (userProfile: User) => {
      axios({
        method: 'PUT',
        url: `${Const.FIREBASE_ENDPOINT}`,
        data: userProfile
      }).then((response) => {
        dispatchPanelState({
          type: ActionType.SUBMITTED,
          payload: response.data
        }
      )}).catch((error) => {
        dispatchPanelState({
          type: ActionType.FAILED,
          payload: userProfile
        });

        setErrorState({
          status: Const.HTTP_PUT_ERROR,
          message: error.message
        });

      });
    }, []);

  /**
   * Function handling edit button to dispatch for changing Panel State
   */
  const editUserProfileHandler = useCallback(
    () => {
      dispatchPanelState({
        type: ActionType.MODIFYING,
        payload: {
          ...panelState.user
        }
      })
    },[panelState.user]);

  /**
   * Function handling submit button to dispatch for changing Panel State,
   * and to update user information by http PUT request
   * 
   * @param {object: User} userProfile - User information to update
   */
  const sumbitUserProfileHandler = useCallback(
    (userProfile: User) => {

      dispatchPanelState({
        type: ActionType.SUBMITTING,
        payload: userProfile
      })
      
      updateUserInfo(userProfile);

  },[updateUserInfo]);

  /**
   * Function handling retry button to be shown on error,
   * and dispatching Panel State as well as making HTTP request
   * based on error status
   * 
   * @param {object: User} userProfile - User information to update
   */
  const retryUserProfileHandler = useCallback(
    (userProfile: User) => {
      if(errorState.status === Const.HTTP_GET_ERROR) {
        dispatchPanelState({
          type: ActionType.RETRIEVING,
          payload: {
            ...panelState.user,
          }
        })
    
        retriveUserInfo();  
      } else if(errorState.status === Const.HTTP_PUT_ERROR) {
        dispatchPanelState({
          type: ActionType.SUBMITTING,
          payload: userProfile
        })
        
        updateUserInfo(userProfile);  
      }

    },[retriveUserInfo, updateUserInfo, panelState.user, errorState.status]);

  // Constructing and caching inner components
  const UserProfileForm = useMemo(() => {
    return (
      <InputForm
        userProfile={panelState.user}
        onSubmit={sumbitUserProfileHandler}/>
    )
  }, [panelState.user, sumbitUserProfileHandler]);

  const UserProfileDesc = useMemo(() => {
    return (
      <Description
        userProfile={panelState.user}
        onEdit={editUserProfileHandler}/>
    )
  }, [panelState.user, editUserProfileHandler]);

  const UserImage = useMemo(() => {
    return (
      <ProfileImage emailAddress={panelState.user.emailAddress}/>
    )
  }, [panelState.user.emailAddress]);

  // Chanigng UI based on status in PanelState
  if(panelState.status === 'NOT_EDITABLE') {
    panelTopContent = UserImage;
    panelBottomContent = UserProfileDesc;
  } else if (panelState.status === 'EDITABLE') {
    panelTopContent = UserImage;
    panelBottomContent = UserProfileForm;
  } else if (panelState.status === 'LOADING') {
    panelTopContent = <LoadingIndicator/>;
  } else if (panelState.status === 'RETRY') {
    panelTopContent = <Notification errorMessage={errorState.message}/>;
    panelBottomContent = <Button type='button' marginTop="2rem" onClick={() => retryUserProfileHandler(panelState.user)}>Retry</Button>;
  }

  useEffect(() => {
    dispatchPanelState({
      type: ActionType.RETRIEVING,
      payload: {
        ...panelState.user,
      }
    })
    retriveUserInfo();
  }, [retriveUserInfo]);

  return (
    <div className='panel'>
      {panelTopContent}
      {panelBottomContent}
    </div>
  );
};

export default Panel;
