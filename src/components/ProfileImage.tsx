// Load from packages
import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faUser } from '@fortawesome/free-solid-svg-icons'

// Load from source code
import ImageUtil from '../utils/ImageUtil'
import ProfileImageProps from '../types/ProfileImageProps';
import './ProfileImage.scss';

/**
 * Functional component to show an image on profile
 * 
 * @param {ProfileImageProps} props - property values from the parents
 */
const ProfileImage = (props: ProfileImageProps) => {  
  return (
    <div className="profileimage">
      {
        props.emailAddress ? 
        <div
          className="profileimage-picture-dynamic"
          style={{
            backgroundImage: `url(${ImageUtil.getGravatarsUrl(props.emailAddress)})`
          }}>
        </div> :
        <FontAwesomeIcon
          color="#d4d4d4"
          size="6x"
          className="profileimage-picture-default"
          icon={faUser} />
      }
    </div>
  );
};

export default ProfileImage;
