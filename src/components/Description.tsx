// Load from packages
import React from 'react';

// Load from source code
import Button from './Button';
import DescriptionProps from '../types/DescriptionProps';
import './Description.scss';

/**
 * Functional component to show a description for user
 * 
 * @param {DescriptionProps} props - property values from the parents
 */
const Description = (props: DescriptionProps) => {

  return (
    <div
      className="desc">
        <div
          className="desc--field">
          <div
            className="desc--label">
            EMAIL ADDRESS
          </div>
          <div>
            {props.userProfile.emailAddress? props.userProfile.emailAddress: 'N/A'}
          </div>
        </div>
        <div
          className="desc--field">
          <div
            className="desc--label">
            NAME
          </div>
          <div>
            {props.userProfile.firstName} {props.userProfile.lastName}
          </div>
          <Button
            type='button'
            marginTop="4rem"
            onClick={props.onEdit}>
            Edit
          </Button>
        </div>
    </div>
  );
};

export default Description;
