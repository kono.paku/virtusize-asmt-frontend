// Load from packages
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faBell } from '@fortawesome/free-solid-svg-icons'

// Load from source code
import NotificationProps from '../types/NotificationProps';
import './Notification.scss';

/**
 * Functional component to show an notification for error on http request
 * 
 * @param {NotificationProps} props - property values from the parents
 */
const Notification = (props: NotificationProps) => {
  return (
    <div
      className="notification">
      <FontAwesomeIcon
        color="#d4d4d4"
        size="2x"
        className="profileimage-picture-default"
        icon={faBell} />
        {props.errorMessage}
        <br/>
        Please retry!
    </div>
  );
};

export default Notification;
