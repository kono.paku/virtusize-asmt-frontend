// Load from packages
import React, { useEffect, useRef, useState  } from 'react';
import { Formik, Form, Field } from 'formik';
import * as Yup from 'yup';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faEye } from '@fortawesome/free-solid-svg-icons'
import zxcvbn from 'zxcvbn';

// Load from source code
import Button from './Button';
import StrenghIndicator from './StrenghIndicator';
import Const from '../utils/Const';
import InputFormProps from '../types/InputFormProps';
import './InputForm.scss';

/**
 * Functional component to show a input form
 * 
 * @param {InputFormProps} props - property values from the parents
 */
const InputForm = (props: InputFormProps) => {
  const inputRef = useRef<HTMLInputElement>(null);
  const [isPasswordVisible, setPasswordVisibility] = useState(true);
  
  /**
   * Handle an eyeicon click eventto make password visble
   */
  const handleEyeIconClick = () => {
    setPasswordVisibility(isPasswordVisible? false : true);
  };

  // Set FormSchem for validation from Formik
  const FormSchema = Yup.object().shape({
    firstName: Yup.string()
      .min(Const.NAME_MIN_LENGTH, Const.SHORT_MSG)
      .max(Const.NAME_MAX_LENGTH, Const.LONG_MSG)
      .required(Const.REQUIRED_MSG),
    lastName: Yup.string()
      .min(Const.NAME_MIN_LENGTH, Const.SHORT_MSG)
      .max(Const.NAME_MAX_LENGTH, Const.LONG_MSG)
      .required(Const.REQUIRED_MSG),
    emailAddress: Yup.string()
      .email(Const.INVALID_EMAIL_MSG),
    password: Yup.string()
      .min(Const.PASSWORD_MIN_LENGTH, Const.SHORT_MSG)
      .required(Const.REQUIRED_MSG)
  });
 
  useEffect(() => {
    if (inputRef.current) {
      inputRef.current.focus();
    }
  },[]);

  return (
    <Formik
      initialValues={props.userProfile}
      validationSchema={FormSchema}
      onSubmit={user => {props.onSubmit(user)}}>
    {({
      values,
      errors,
      touched,
    }) => (
      <Form
        className='inputform'>
        <div className='inputform--control-text'>
          <Field
            className='inputform--input-email'
            innerRef={inputRef}
            name='emailAddress'
            id='emailAddress'
            type='email'
            placeholder='Email Address'/>
          {
            errors.emailAddress && touched.emailAddress ?
            <div className="inputform--validation-msg">
              {errors.emailAddress}
            </div> : null
          }
        </div>
        <div className='inputform--control-text'>
          <div className='inputform--input-name'>
          <Field
            className='inputform--input-firstname'
            type='text'
            name='firstName'
            id='firstName'
            placeholder='First Name'/>
          {
            errors.firstName && touched.firstName ? 
            <div className="inputform--validation-msg">
              {errors.firstName}
            </div> : null
          }
          </div>
          <div className='inputform--input-name'>
            <Field
              className='inputform--input-lastname'
              type='text'
              name='lastName'
              id='lastName'
              placeholder='Last Name'/>
            {
              errors.lastName && touched.lastName ?
              <div className="inputform--validation-msg">
                {errors.lastName}
              </div> : null
            }
          </div>
          </div>
          
        <div
          className='inputform--control-text'>
          <Field
            className='inputform--input-password'
            type={isPasswordVisible ? 'password' : 'text'}
            name='password'
            id='password'
            placeholder='Password'/>
          {
            errors.password && touched.password ?
            <div className="inputform--validation-msg">
              {errors.password}
            </div> : null
          }
          <FontAwesomeIcon
            color={isPasswordVisible? '#d4d4d4':'#4BD5BF'}
            size='2x'
            className='inputform--input-mask'
            onClick={handleEyeIconClick}
            icon={faEye} />
          {
            values.password && values.password.length >= Const.PASSWORD_MIN_LENGTH ?
            <StrenghIndicator score={zxcvbn(values.password).score}/> : null
          }
          <div
            className='inputform--strong-indicator'>
          </div>
        </div>
        <div className='inputform--button'>
          <Button
           type='submit'
           marginTop='15rem'>
           Submit
         </Button>
        </div>
      </Form>
      )}
      </Formik>
  );
};

export default InputForm;
